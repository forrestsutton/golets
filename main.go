package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)

//define a home handler function which write a byte slce conaint 
func home (w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("hello from Snippetbox"))
}

func showSnippet(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil || id < 1 {
	    http.NotFound(w,r)
	    return
	}
	fmt.Fprintf(w, "display a specific snippet with ID %d", id)
}


func createSnippet(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Create a new snippet..."))
}
func main() {
        mux := http.NewServeMux()
	mux.HandleFunc("/", home)
	mux.HandleFunc("/snippet", showSnippet)
	mux.HandleFunc("/snippet/create", createSnippet)
	// use the http.list and serv function to statr a new web server
	//http.ListenAndServe() return an error
	//we use the log.Fatal() function to log the error message and exit
	log.Println("Starting server on :4000")
	err := http.ListenAndServe(":4000", mux)
	log.Fatal(err)
}
